//
// Generate by EzInkscape
// Language: m
// Date: 30/08/2014 23:04:44
//

#import "Assets.h"

@implementation Assets


// Menu
const NSString *kBackgroundMenu = @"BackgroundMenu.png";
const float kBackgroundMenu_X = 159.91964636;
const float kBackgroundMenu_Y = 239.2293675;

const NSString *kButtonStart = @"ButtonStart.png";
const float kButtonStart_X = 159.919649;
const float kButtonStart_Y = 239.22941;

// Logo
const NSString *kLogo = @"Logo.png";
const float kLogo_X = 159.919647;
const float kLogo_Y = 409.6304705;

// Objects
const NSString *kObjectA = @"ObjectA.png";
const float kObjectA_X = 99.919647;
const float kObjectA_Y = 100.22461;

const NSString *kObjectB = @"ObjectB.png";
const float kObjectB_X = 139.91965;
const float kObjectB_Y = 100.22461;

const NSString *kObjectC = @"ObjectC.png";
const float kObjectC_X = 179.91965;
const float kObjectC_Y = 100.22461;

const NSString *kObjectD = @"ObjectD.png";
const float kObjectD_X = 219.91965;
const float kObjectD_Y = 100.22461;

@end
